const functions = require('firebase-functions')
const os = require('os')
const path = require('path')
const cors = require('cors')({ origin: true })
const Busboy = require('busboy')
const fs = require('fs')
const axios = require('axios')
const FormData = require('form-data')
const admin = require('firebase-admin')
admin.initializeApp()

const firestore = admin.firestore()

const API_URL = 'https://europe-west1-promiseq-production2.cloudfunctions.net/sendToThreatDetect'
const PROMISEQ_SUBSCRIPTION_KEY = '317e3ebb-8ced-4536-a619-da'

exports.uploadImage = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    if (req.method !== 'POST') {
      return res.status(405).end()
    }

    const busboy = new Busboy({ headers: req.headers })
    let uploadData = null

    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      const filepath = path.join(os.tmpdir(), filename)
      uploadData = { file: filepath, type: mimetype }
      file.pipe(fs.createWriteStream(filepath))
    })

    busboy.on('finish', async () => {
      if (!uploadData) {
        return res.status(400).send('No file uploaded')
      }
      const formData = new FormData()

      formData.append('file', fs.createReadStream(uploadData.file), {
        filename: path.basename(uploadData.file),
        contentType: uploadData.type
      })
      try {
        const apiResponse = await axios.post(API_URL, formData, {
          headers: {
            ...formData.getHeaders(),
            'promiseq-subscription-key': PROMISEQ_SUBSCRIPTION_KEY
          }
        })

        const detectedObjects = apiResponse.data

        const docRef = firestore.collection('detections').doc()
        await docRef.set(detectedObjects)

        return res.status(200).send(detectedObjects)
      } catch (error) {
        console.error('Error:', error)
        return res.status(500).send(error.message)
      }
    })

    busboy.end(req.rawBody)
  })
})
