export async function uploadImageToCloudFunction(file) {
  const formData = new FormData()
  formData.append('image', file)

  const response = await fetch(
    'https://us-central1-object-detection-9d260.cloudfunctions.net/uploadImage',
    {
      method: 'POST',
      body: formData
    }
  )

  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`)
  }

  return response.json()
}
